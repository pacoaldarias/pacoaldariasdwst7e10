<?php
// si cerramos el navegador SI guarda el acumulado 1 AÑO
if (isset($_COOKIE['contador'])) { // Definido la cookie
    setcookie('contador', $_COOKIE['contador'] + 1, time() + 365 * 24 * 60 * 60);
    $mensaje = 'Número de visitas: ' . $_COOKIE['contador'];
} else {
    // Caduca en un año
    setcookie('contador', 1, time() + 365 * 24 * 60 * 60);
    $mensaje = "Bienvenido";
}
?>
<!--
uso correcto. hay que recargar
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php echo $mensaje; ?>
    </body>
</html>
